﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Runtime.CompilerServices;
using System.Text.RegularExpressions;

namespace Ch27
{
  class Program
  {

    static void Main(string[] args)
    {

      foreach (var word_freq in count_and_sort(args[0]))
      {
        Console.WriteLine("-----------------------------");
        foreach (var wc in word_freq.Take(25))
          Console.WriteLine($"{wc.Key} - {wc.Value}");
      }
    }

    static IEnumerable<char> characters(string filename)
    {
      foreach (var line in File.OpenText(filename).ReadToEnd().Split('\n'))
        foreach (var c in line)
          yield return c;
    }

    static IEnumerable<string> all_words(string filename)
    {
      var start_char = true;
      var word = "";
      foreach (var c in characters(filename))
      {

        if (start_char)
        {
          word = "";
          if (char.IsLetterOrDigit(c))
          {
            word += char.ToLower(c);
            start_char = false;
          }
          else continue;
        }
        else
        {
          if (char.IsLetterOrDigit(c))
            word += char.ToLower(c);
          else
          {
            start_char = true;
            yield return word;
          }
        }
      }
    }

    static IEnumerable<string> non_stop_words(string filename)
    {
      var stopwords = Regex.Split(File.OpenText("../stop_words.txt").ReadToEnd(), "[\\W_]+").ToList();
      foreach (var w in all_words(filename))
        if (w.Length > 1 && !stopwords.Contains(w))
          yield return w;
    }

    static IEnumerable<List<KeyValuePair<string, int>>> count_and_sort(string filename)
    {
      var freqs = new Dictionary<string, int>();
      var i = 0;

      foreach(var w in non_stop_words(filename))
      {

        if (!freqs.ContainsKey(w))
          freqs.Add(w, 1);
        else
          freqs[w]++;

        var sorted = freqs.ToList();
        sorted.Sort((kv1, kv2) => kv2.Value - kv1.Value);

        if (i%5000 == 0)
          yield return sorted;

        i++;

        yield return sorted;
      }
    }

  }
}



﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text.RegularExpressions;
using Mono.Data.Sqlite;

namespace Ch25
{
  class Program
  {
    static void Main(string[] args)
    {
      var db_exists = File.Exists("Ch25.db");

      //var ds = "Data Source=:memory:";
      var ds = "Data Source=Ch25.db;Version=3";

      using (var con = new SqliteConnection(ds))
      {
        con.Open();
        if (!db_exists)
        {
          create_db_schema(con);
          load_file_into_database(args[0], con);
        }

        var cmdSelSql = new SqliteCommand(
          "SELECT value, count(*) as C FROM words GROUP BY " +
          "value ORDER BY C DESC", con);

        var rdr = cmdSelSql.ExecuteReader();

        for (var i = 0; i < 25; ++i)
          if(rdr.Read())
            Console.WriteLine($"{rdr.GetString(0)} {rdr.GetInt32(1)}");

        con.Close();
      }
    }

    static void create_db_schema(SqliteConnection conn)
    {
      new SqliteCommand("CREATE TABLE documents (id INTEGER PRIMARY KEY AUTOINCREMENT, name)", conn).ExecuteScalar();
      new SqliteCommand("CREATE TABLE words (id, doc_id, value)", conn).ExecuteScalar();
      new SqliteCommand("CREATE TABLE characters (id, word_id, value)", conn).ExecuteScalar();
    }

    static void load_file_into_database(string path_to_file, SqliteConnection conn)
    {
      var words = _extract_words(path_to_file);
      var cmdDocs = new SqliteCommand("INSERT INTO documents (name) VALUES (?)", conn);
      cmdDocs.Parameters.Add(new SqliteParameter("@name", path_to_file));
      cmdDocs.ExecuteNonQuery();

      var cmdSelDoc = new SqliteCommand("SELECT id FROM documents WHERE name=?", conn);
      cmdSelDoc.Parameters.Add(new SqliteParameter("@name", path_to_file));
      var rdr = cmdSelDoc.ExecuteReader();
      var read = rdr.Read();
      var doc_id = rdr.GetInt32(0);

      cmdSelDoc = new SqliteCommand("SELECT MAX(id) from words", conn);
      rdr = cmdSelDoc.ExecuteReader();
      read = rdr.Read();
      int word_id;
      try { word_id = rdr.GetInt32(0); }
      catch (InvalidCastException e) { word_id = 0; }

      Console.WriteLine("Processing . . .");
      foreach (var w in words)
      {
        Console.Write($"  Inserting: {w}");
        var cmdIns = new SqliteCommand("INSERT INTO words VALUES(?, ?, ?)", conn);
        cmdIns.Parameters.AddRange(new []
        {
          new SqliteParameter("@id", word_id),
          new SqliteParameter("@doc_id", doc_id),
          new SqliteParameter("@value", w),
        });
        cmdIns.ExecuteNonQuery();

        var char_id = 0;
        foreach (var @char in w)
        {
          cmdIns = new SqliteCommand("INSERT INTO characters VALUES(?,?,?)", conn);
          cmdIns.Parameters.AddRange(new []
          {
            new SqliteParameter("@id", char_id),
            new SqliteParameter("@word_id", word_id),
            new SqliteParameter("@value", @char),
          });
          cmdIns.ExecuteNonQuery();
          char_id++;
        }
        word_id++;
        Console.WriteLine(" - [Done]");
      }
    }

    static string[]_extract_words(string path_to_file)
    {
      using (var f = File.OpenText(path_to_file))
      {
        var str_data = f.ReadToEnd().ToLower();
        var word_list = Regex.Split(str_data, "[\\W_]+");

        using (var stops = File.OpenText("../stop_words.txt"))
        {
          var stop_words = stops.ReadToEnd().Split(',');
          return word_list.Where(w => w.Length > 1 && !stop_words.Contains(w)).ToArray();
        }
      }
    }

  }
}


